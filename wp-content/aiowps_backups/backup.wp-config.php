<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *

// * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define( 'WP_MEMORY_LIMIT', '512M' );

//define('WP_HOME','http://wardrummer.hyperbig.com');
//define('WP_SITEURL','http://wardrummer.hyperbig.com');

/** The name of the database for WordPress */
define( 'DB_NAME', 'deacontv_livedb' );

//define( 'WP_MEMORY_LIMIT', '256M' );
/** MySQL database username */
define( 'DB_USER', 'deacontv_user' );

/** MySQL database password */
define( 'DB_PASSWORD', '*H]vPR],0@bV' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** DB Write Permission */
define('FS_METHOD','direct');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
/*
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );
*/

define('AUTH_KEY',         '^)]Io[hQ:x>nT*IpYjs%-B7pa.&ZDcOY)6!hSm]Ws7H#s9xj?PjMyd%7wtKVc)|t');
define('SECURE_AUTH_KEY',  'R=z#$p%6SA1|kO.pAwrmd=_~eI`;fUDv)5V^-85X;v}bLU`7sLb75pLyM@`-%)N9');
define('LOGGED_IN_KEY',    'GrZ`E^Bz,yT{p~,q5MBp2Qa{y-8!=/?)S-+ES=/&?xTlSh}.)C}a*^^4A]16c_~b');
define('NONCE_KEY',        'EW_Gv@b1lAv:=7dXEJPF2]`xN5/88g6zGrNWd=BEY5IlKFs|@:^BC]m2.t4EEt>k');
define('AUTH_SALT',        'BAv76zZlixnfdj+I}J`AaSLhB1?ThGi||:0utGU -tm6!e 0BaUD|0QXJcAH-8Aa');
define('SECURE_AUTH_SALT', '|<)!8pxUJKw~bch%cl qj;S]7nL4-;L+]}rhG0e?.K#)@M=j_b=5{5(fqU,jX nL');
define('LOGGED_IN_SALT',   'b2! FYq0gLbQ*Jiazm5SqW=nL=Z5=nb#W-YN?S{S5FwJX+ysJ[O(aeg593kXD}:|');
define('NONCE_SALT',       ':XGumZ$2zT<5Id53(fAz^FzY0MDx^++#MBG}#fwJ2oz|d_!RJv(qu!.6>3Em>}sm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define( 'WP_AUTO_UPDATE_CORE', false );
define( 'DISALLOW_FILE_EDIT', true );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
