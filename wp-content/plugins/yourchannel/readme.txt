=== YourChannel: Everything you want in a YouTube plugin. ===
Contributors: plugin.builders
Tags: youtube, youtube player, youtube gallery, youtube playlists, stream, embed, video, live stream, player, playlist, youtube plugin, youtube player, youtube gallery, youtube channel, search, ratings, views, youtube playlist, curate, audio, podcast, music
Donate link: http://plugin.builders/yourchannel/?d=donate
Requires at least: 3.5
Tested up to: 5.2.1
Stable tag: 0.9.9.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl.html

Setup beautiful YouTube feed streams with 1 copy paste & 2 clicks. Displays banner, uploads, playlists and more (All optional). You just need a YouTube username.

== Description ==
Do you create or curate YouTube videos? Now turn your website with YouTube links into an engagement platform.

You don't need more storage or bandwidth, and your visitors hate leaving the webpage they've already loaded, so why don't you show your videos and playlists in your own website.
	
Tell this plugin your YouTube username or channel ID and it'll setup your channel in your own property.

**What it displays :**

* Banner image, profile picture and username.
* Total videos and views.
* Single videos.
* Uploads.
* Playlists (lists videos on that playlist on click).
* Banner image in footer.
* All of above are optional. You can choose what to display.
* Default color accent Black & White.

**Features :**

* Easy visual builder with live preview which generates shortcode that looks like `[yourchannel user="user"]`.
* Play video in Lightbox or Inline.
* 7 video thumb styles.
* RTL support.
* Beautifully responsive (IT'S 2019).
* Font icons.
* Feature suggestions are implemented quickly.
* Smooooth transitions.
* Shortcode works in page, posts or custom fields.
* Loads files only if shortcode is present. 
* Quick translation form for UI terms (Videos, Playlists, Loading... etc.).
* Translation ready.
* Autoplay option.
* Easy shortcode instructions in post editor.
* Cache system for fast loading.

**[Much more features are available in PRO version](https://plugin.builders/yourchannel/#compare)**


Don't let visitors leave your site for YouTube.

**[Demo](http://plugin.builders/yourchannel/demo/?demo=wp.org)**

**[Video tutorial in Spanish](http://www.webempresa.com/blog/item/1733-publica-tu-canal-de-videos-de-youtube-en-wordpress.html)**

**Support :** We'll respond to your issue within 2-18 hours.

Write to us at **support [at] plugin.builders**

**Note :** This plugin cannot list unlisted videos/playlists.

== Installation ==
Upload YourChannel folder to your wp-content/plugins/ directory.
Activate the plugin through the 'Plugins' menu in WordPress.
Go to Settings > YourChannel.
Fill in the inputs with your API key, (we've left our API key for you to get started, but you should get your own) YouTube username and some other preferences.
Save.

Put the shortcode generated anywhere in your posts. (Also supports custom fields.)

== Changelog ==

= 1.4 =
* Added shortcode instructions on YourChannel page.
* Minor updates for latest WordPress release.

= 1.3 =
* Fixed a bug with search and sidebar playlist autoplay.

= 1.2 =
* Added many many new shortcode options.

= 1.1 =
* Fixed bugs that caused conflict with some page builder and SEO plugins.
* Added button to refresh cache.
* Added Duplicate button.

= 1.0 =
* Implemented cache system to load faster and make less YouTube API calls.
* Added 3 new themes: Slider, Carousel & List.
* Rich single videos ( with title, stats, description etc ). Shortoode e.g: `[yourchannel video="C8WOH3eoCMw"]`

= 0.8 =

* Improved grid layout - no ugly margins.
* Blacklist videos.
* Improved Prev / Next pagination.
* Specify grid column numbers.
* Choose default tab: Videos, Playlists or Search.
* ** Licensing system: Please update the plugin to receive future updates. **

= 0.6.15 =
More iOS issues fixed, and made compatible with new WP release.

= 0.6.10 =
Fixed playback issues on iOS devices.

= 0.3.1 =
Playlists are optional.
If playlists not shown, menu'll also be hidden.
Default colors accents is now Black & White.

= 0.3 =
Now also accepts Channel ID.

= 0.1 =
Initial version.
